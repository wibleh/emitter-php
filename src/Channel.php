<?php

namespace Emitter;

use Emitter\Exceptions\EmitterException;
use GuzzleHttp\Client;

class Channel
{
    protected $secretKey;
    protected $name;
    protected $type;
    protected $ttl;
    protected $client;

    public function __construct($secretKey, $name, $type, $ttl)
    {
        $this->secretKey = $secretKey;
        $this->name = $name;
        $this->type = $type;
        $this->ttl = $ttl;

        $this->validateConfiguration();
    }

    protected function validateConfiguration()
    {
        if ($this->secretKey == '') {
            throw new EmitterException('Secret key is missing');
        }

        if (!preg_match('/^(([0-9A-Za-z\.\:\-]+)+\/?)', $this->name)) {
            throw new EmitterException(sprintf('Invalid channel name: %s', $this->name));
        }

        if (!preg_match('/^[rwls]+$/', $this->type)) {
            throw new EmitterException(sprintf('Invalid channel type: %s', $this->type));
        }


    }

    public function getClient()
    {
        if (!$this->client) {
            $this->client = new Client();
        }

        return $this->client;
    }

    public function publish($message)
    {
        $this->getClient()->publish($message);
    }

    public function store($message)
    {
        $this->getClient()->store($message);
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getHistory($quantity = 10)
    {
        return $this->getClient()->history($quantity);
    }

    public function set
}
