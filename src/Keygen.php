<?php

namespace Emitter;

use Emitter\Exceptions\EmitterException;

class Keygen
{
    protected $secretKey;
    protected $channel;
    protected $type;
    protected $ttl;

    public function __construct($secretKey, $channel, $type = 'rwls', $ttl = 300)
    {
        $this->secretKey = $secretKey;
        $this->channel = $this->setChannel($channel);
        $this->type = $this->setType($type);
        $this->setTtl($ttl);
    }

    public function setChannel($channel)
    {
    }

    public function generateKey()
    {

    }
}
