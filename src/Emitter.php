<?php

namespace Emitter;

use Emitter\Exceptions\EmitterException;

class Emitter
{
    protected $secretKey;
    protected $channels = [];

    public function __construct($secretKey)
    {
        $this->secretKey = $secretKey;
    }

    public function createChannel($name, $type = 'rwls', $ttl = 1200)
    {
        $channel = new Channel($this->secretKey, $name, $type, $ttl);

        return $this->channels[] = $channel;
    }

    public function getChannel($name, $type)
    {
        foreach ($this->channels as $channel) {
            if ($channel->matchName($name) &&  $channel->matchType($type)) return $channel;
        }

        return $this->createChannel($name);
    }

    public function publish($message, $channelName)
    {
        $channel = $this->getChannel($channelName, 'w');

        return $channel->publish($message);
    }

    public function store($message, $channelName)
    {
        $channel = $this->getChannel($channelName, 's');

        return $channel->store($message);
    }

    public function read($channelName, $quantity)
    {
        $channel = $this->getChannel($channelName, 'r');

        return $channel->read($quantity);
    }

    public function
}
